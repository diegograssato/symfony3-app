<?php

echo <<<EOB
		
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View cache information</title>
    <style type="text/css">
	 
	pre {margin: 0px; font-family: monospace;}
	a:link,a:visited {color: #000099; text-decoration: none;}
	a:hover {text-decoration: underline;}
	table {border-collapse: collapse; width:100%}
	.center {text-align: center;}
	.center table { margin-left: auto; margin-right: auto; text-align: left;}
	.center th { text-align: center !important; }
	.middle {vertical-align:middle;}
	td, th { border: 1px solid #000; font-size: 16pt;height:30px}
	tbody tr:nth-child(even) {
        background-color: #eee;
    }
	h1 {font-size: 150%;}
	h2 {font-size: 100%;}
	.p {text-align: left;}
	.e {background-color: #ccccff; font-weight: bold; color: #000; width:50%; white-space:nowrap;}
	.h {background-color: #9999cc; font-weight: bold; color: #000;}
	.v {background-color: #cccccc; color: #000;}
	.vr {background-color: #cccccc; text-align: right; color: #000; white-space: nowrap;}
	.b {font-weight:bold;}
	.white, .white a {color:#fff;}
	img {float: right; border: 0px;}
	.content {
            position: absolute; 
            background: white;
            border: 1px solid #ccc;
            height: 50%;
            width: 450px;
             
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-align-content: center;
            align-content: center;
             margin-left: 25%; 
        }
        .center {
            text-align: center; 
            width:100%;
        } 
	 
</style>
</head>
<body class="content">
<div class="center">
        <h1>Cache View Information</h1>
		<table cellspacing=0><tbody>
		<tr>
		    <th><a href="opcache.php">Opcache View</a> </th> 		     
		</tr>
		
		<tr>
		    <th><a href="opcache-gui.php">Opcache GUI</a> </th>  
		</tr>
		
		<tr>
		    <th><a href="ocp.php">Opcache Control Panel</a> </th>  
		</tr>
		
    <tr>
    <th><a href="apc.php">APCu Cache Information</a> </th>  
</tr>
<tr>
		    <th>
Runnig XHGui<br> 
EOB;


echo 'cd '.__DIR__.'/cache/xhgui';
echo <<<EOB
		
	        <br/>composer install
	        <br/> 
		    php  -S  0.0.0.0:8888  -t  webroot
		    </th>  
		</tr>
	 </tbody>
 </table>
  <h2>Power by DTuX</h2>
</div>
</body>
</html>
EOB;
