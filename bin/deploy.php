<?php
if ((!$loader = includeIfExists(__DIR__.'/vendor/autoload.php')) && (!$loader = includeIfExists(__DIR__.'/../../autoload.php'))) {
    die('You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -sS https://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL);
}
$startTime = getrusage();
// All Deployer recipes are based on `recipe/common.php`.
require __DIR__.'/deploy/symfony.php';

// Define a server for deployment.
serverList(__DIR__.'/deploy/servers.yml');

$endTime = getrusage();

task('done', function () use ($startTime, $endTime) {
    $runTime =  ($endTime["ru_utime.tv_sec"] * 1000 + intval($endTime["ru_utime.tv_usec"] / 1000))
        - ($startTime["ru_utime.tv_sec"] * 1000 + intval($startTime["ru_utime.tv_usec"] / 1000));

    $msg = sprintf('Execution Time: %ss', $runTime);

    writeln($msg);
});

task('success', function () {
    writeln("<info>Successfully deployed!</info>");
})->once()->setPrivate();

// Specify the repository from which to download your project's code.
// The server needs to have git installed for this to work.
// If you're not using a forward agent, then the server has to be able to clone
// your project from this repository.
set('repository', 'git@github.com:slashfan/LexikJWTAuthenticationBundleSandbox.git');
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:create_cache_dir',
    'deploy:shared',
    'deploy:vendors',
    'deploy:cache:warmup',
    'deploy:symlink',
    'cleanup',
    'done'
])->desc('Deploy your project');

after('deploy', 'success');
