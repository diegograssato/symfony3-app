<?php
require_once 'AppKernel.php';

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class DockerAppKernel extends AppKernel
{
    const SF_SHM_CACHE_FOLDER='/dev/shm/symfony_app/';

    public function setupShm($type)
    {
        $dir = self::SF_SHM_CACHE_FOLDER . $type;
        if (!is_dir($dir) && strlen($dir)>0){
            @mkdir($dir, 0777, true);
        }
    }

    public function getCacheDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return self::SF_SHM_CACHE_FOLDER . 'cache/' .  $this->environment;
        }

        return parent::getCacheDir();
    }

    public function getLogDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return self::SF_SHM_CACHE_FOLDER . 'logs';
        }

        return parent::getLogDir();
    }
}
